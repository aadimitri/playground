import shutil
import sys
import os.path
import subprocess

HIDE = True

def install(package, manager):
    if manager == "pip":
        subprocess.check_call([sys.executable, "-m", manager, "install", package], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    elif manager == "conda":
        subprocess.check_call([sys.executable, "-m", manager, "install", "-c conda-forge", package], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

def check_installation(package):
    # Check if ipopt is installed
    try:
        subprocess.check_output(["conda", "list", package], stderr=subprocess.STDOUT)
        return True
    except subprocess.CalledProcessError:
        return False

# # Define the command as a list of arguments
# command = ['conda', 'update', '-n', 'base', 'conda']

# # Execute the command and hide the output
# subprocess.call(command, stdout=subprocess.DEVNULL)
        
# check if pyomo has been installed. If not, install with pip
pip_packages = ["pyomo"]
for package in pip_packages:
    if not shutil.which(package):
        install(package, "pip")
    assert(shutil.which(package))

# check if ipopt is installed. If not, install.
conda_packages = ["ipopt"]
for package in conda_packages:
    if not (shutil.which(package) or os.path.isfile(package)):
        try:
            install(package, "conda")
        except:
            pass
    # Call the check_ipopt_installation() function to check if ipopt is installed
    if check_installation(package):
        print(f"{package} is installed")
    else:
        print(f"{package} is not installed or installation is incomplete")

# check if COIN-OR CBC is installed. If not, install.
if not (shutil.which("cbc") or os.path.isfile("cbc")):
    try:
        install("coincbc", "conda")
    except:
        pass
    # Call the check_ipopt_installation() function to check if ipopt is installed
    if check_installation("cbc"):
        print("cbc is installed")
    else:
        print("cbc is not installed or installation is incomplete")

import pyomo.environ as pyo