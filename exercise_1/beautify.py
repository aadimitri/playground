import os
import pandas as pd
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle

# Grading scale dictionary
grading_scale = {
    (100, 94.9): ("1,0", "sehr gut"),
    (94.9, 89.5): ("1,3", "sehr gut (-)"),
    (89.5, 84.3): ("1,7", "gut (+)"),
    (84.3, 79.0): ("2,0", "gut"),
    (79.0, 73.7): ("2,3", "gut (-)"),
    (73.7, 68.2): ("2,7", "befriedigend (+)"),
    (68.2, 63.1): ("3,0", "befriedigend"),
    (63.1, 57.9): ("3,3", "befriedigend (-)"),
    (57.9, 52.6): ("3,7", "ausreichend (+)"),
    (52.6, 50.0): ("4,0", "ausreichend"),
    (50.0, 0.0): ("5,0", "nicht bestanden")
}


def get_grade(percentage):
    for (upper, lower), (grade, pred) in grading_scale.items():
        if lower <= percentage <= upper:
            return grade, pred
    return "5,0", "nicht bestanden"


def get_student_info(filename):
    basename = os.path.basename(filename)
    name, _ = os.path.splitext(basename)
    parts = name.split("_")
    firstname = parts[0]
    lastname = parts[1]
    matr_nr = parts[2]
    exercise_nr = parts[3]  # Extract exercise number from filename
    return lastname, firstname, matr_nr, exercise_nr


def create_pdf(final_grades_file):
    # Read CSV file
    df = pd.read_csv(final_grades_file)

    # Extract task names from CSV file
    task_columns = [col for col in df.columns if col.startswith("task")]

    # Define column names for the PDF table
    columns = ["Lastname", "Firstname", "MATR.-NR."] + task_columns + ["Final Percentage", "Grade"]

    # Initialize data for the PDF table
    data = []

    # Define alternating row colors
    color1 = colors.HexColor("#FFFFE0")  # Light yellow
    color2 = colors.HexColor("#FFEFD5")  # Papaya whip
    alternating_colors = [color1, color2]

    exercise_nr = None

    # Populate data for the PDF table
    for index, row in df.iterrows():
        lastname, firstname, matr_nr, exercise_nr = get_student_info(row['file'])
        final_percentage = row['percent_correct'] * 100
        grade, pred = get_grade(final_percentage)
        row_data = [lastname, firstname, matr_nr] + [f"{row[task] * 100:.2f}%" for task in task_columns] + [
            f"{final_percentage:.2f}%", grade]
        data.append(row_data)

    # Sort data based on the last name
    data.sort(key=lambda x: x[0])

    # Create PDF
    pdf_filename = f"Final_Grades_{exercise_nr}.pdf"
    doc = SimpleDocTemplate(pdf_filename, pagesize=letter)
    table = Table([columns] + data)
    table.setStyle(TableStyle([('BACKGROUND', (0, 0), (-1, 0), colors.gray),
                               ('TEXTCOLOR', (0, 0), (-1, 0), colors.white),
                               ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                               ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
                               ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
                               ('GRID', (0, 0), (-1, -1), 1, colors.black)]))

    # Apply alternating row colors
    for i, row_data in enumerate(data):
        color_index = i % len(alternating_colors)
        row_color = alternating_colors[color_index]
        table_style = [('BACKGROUND', (0, i + 1), (-1, i + 1), row_color)]
        table.setStyle(TableStyle(table_style))

    doc.build([table])

    # Create Excel file
    excel_filename = f"Final_Grades_{exercise_nr}.xlsx"
    df_final = pd.DataFrame(data, columns=columns)  # Convert the list of lists to DataFrame
    df_final.to_excel(excel_filename, index=False)


if __name__ == "__main__":
    final_grades_file = "final_grades.csv"
    create_pdf(final_grades_file)
