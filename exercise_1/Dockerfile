FROM ubuntu:22.04

# Avoid prompts during installation
ENV DEBIAN_FRONTEND=noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN=true

# Update package list, install Python 3, pip and otter-grader
RUN apt-get update \
    && apt-get install -y python3-pip \
    && pip install otter-grader

# Install Docker client and daemon
RUN apt-get install -y docker.io \
    && apt install docker-buildx

# Set the working directory inside the container
ARG DOCKER_BUILD_DIR
WORKDIR ${DOCKER_BUILD_DIR}

# Copy the entire contents of the current directory to the container's working directory
COPY . .

# Install any other dependencies
RUN pip install -r requirements.txt

RUN apt update \
    && apt -y install wkhtmltopdf \
    && pip install pdfkit \
    && pip install pypdf \
    && pip install --upgrade pip ipython ipykernel

# Download docker client binary
RUN python-on-whales download-cli